#!/bin/bash

database=$1
username=$2
password=$3

echo "Creating new user & database..."

add_user=""
add_database=""
add_privileges=""

echo "New User: $username"
add_user="CREATE USER $username WITH PASSWORD '$password';"

echo "New Database: $database"
add_database="CREATE DATABASE $database;"
add_privileges="GRANT ALL PRIVILEGES ON DATABASE $database TO $username;"

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    $add_user
    $add_database
    $add_privileges
EOSQL

echo "[DONE]"